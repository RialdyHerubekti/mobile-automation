*** Settings ***
Documentation    Nearest Store
Library          AppiumLibrary

*** Variables ***
${ORDER_FORM}     //android.widget.ImageView[contains(text,”ORDER FROM”)]
${SPRINGLAKE}     //android.widget.TextView[@text="Springlake"]
${GADING_NIAS}    //android.widget.TextView[@text="Gading Nias"]


*** Keywords ***
User see ORDER FROM
    Wait Until Page Contains Element    ${ORDER_FORM}    5s

User choose open store
    Click Element                       ${ORDER_FORM}     
    Wait Until Page Contains element    ${SPRINGLAKE}     30s
    Click Element                       ${GADING_NIAS}
    Sleep                               5s


