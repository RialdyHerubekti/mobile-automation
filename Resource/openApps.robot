*** Settings ***
Documentation    Open Apps Flash Coffee
Library          AppiumLibrary


*** Variables ***
${HOST}        http://localhost:4723/wd/hub
${PLATFORM}    Android
${DEVICE}      emulator-5554
${PACKAGE}     com.flashcoffee 
${ACTIVITY}    com.flashcoffee.SplashActivity

${PERMISSION}         com.android.permissioncontroller:id/permission_allow_foreground_only_button
${GREETING_TEXT}      //android.widget.TextView[@text="DARE TO BE DIFFERENT"]
${GREETING_TEXT_2}    //android.widget.TextView[@text="GET FLASHED"]
${GREETING_TEXT_3}    //android.widget.TextView[@text="BENEFITS JUST FOR YOU"]
${NEXT}               //android.widget.TextView[@text="Next"]
${DONE}               //android.widget.TextView[@text="Done"]
${ORDER_FORM}         //android.widget.TextView[@text="ORDER FROM · 13988km"]


*** Keywords ***
Start Open the Applications
    Open Application    ${HOST}    platformName=${PLATFORM}    deviceName=${DEVICE}    appPackage=${PACKAGE}    appActivity=${ACTIVITY}    automationName=Uiautomator2

User see permission pop up
    Wait Until Page Contains Element    ${PERMISSION}    15s

User allow the permission
    Click Element    ${PERMISSION}

User click next on greeting page
    Wait Until Page Contains Element    ${GREETING_TEXT}      3s
    Click Element                       ${NEXT}
    Wait Until Page Contains Element    ${GREETING_TEXT_2}    3s
    Click Element                       ${NEXT}
    Wait Until Page Contains Element    ${GREETING_TEXT_3}    3s
    CLick Element                       ${DONE}
    Sleep                               5s
