*** Settings ***
Documentation    Change Language
Library          AppiumLibrary

*** Variables ***
${LANGUAGE_BUTTON}     //android.widget.TextView[@text="Language"]
${SIGN_UP}             //android.widget.TextView[@text="SIGN UP / LOGIN"]
${SELECT_LANGUAGE}     //android.widget.TextView[@text="Select Language"]
${BAHASA_INDONESIA}    //android.widget.TextView[@text="Bahasa Indonesia"]
${SAVE_BUTTON}         //android.widget.TextView[@text="Save"]
${BAHASA_BUTTON}       //android.widget.TextView[@text="Bahasa"]
${DAFTAR}              //android.widget.TextView[@text="DAFTAR / MASUK"]


*** Keywords ***
User open hamburger menu
    Swipe By Percent    1    50    60    50    1000

User see "Language" and "SIGN UP / LOGIN" button
    Wait Until Page Contains element    ${LANGUAGE_BUTTON}    30s
    Wait Until Page Contains element    ${SIGN_UP}            30s

User click language button
    Click Element    ${LANGUAGE_BUTTON}
    Sleep            5s

User see text "SELECT LANGUAGE"
    Wait Until Page Contains element    ${SELECT_LANGUAGE}    30s

User choose "Bahasa Indonesia"
    Click Element    ${BAHASA_INDONESIA}
    Sleep            2s

User click save button
    Click Element    ${SAVE_BUTTON}
    Sleep            5s

User see "Bahasa" and "Daftar / Masuk" in side menu
    Swipe By Percent                    1                   50     60    50    1000
    Wait Until Page Contains element    ${BAHASA_BUTTON}    30s
    Wait Until Page Contains element    ${DAFTAR}           30s
    Sleep                               3s
