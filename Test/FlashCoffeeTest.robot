*** Settings ***
Documentation    Flash Coffee Test Automation
Library          AppiumLibrary
Resource         ../Resource/changeLanguage.robot
Resource         ../Resource/nearStore.robot
Resource         ../Resource/openApps.robot
Test Setup       Start Open the Applications
Test Teardown    Close All Applications


*** Test Cases ***
User choose store and change language
    User see permission pop up
    User allow the permission
    User click next on greeting page
    User see ORDER FROM
    User choose open store
    User open hamburger menu
    User see "Language" and "SIGN UP / LOGIN" button
    User click language button
    User see text "SELECT LANGUAGE"
    User choose "Bahasa Indonesia"
    User click save button
    User see "Bahasa" and "DAFTAR / MASUK" in side menu